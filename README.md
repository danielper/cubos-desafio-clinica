# Running MongoDB locally with credentials
`$ docker run -it -d --name mongo \
  -e AUTH=yes \
  -e MONGODB_ADMIN_USER=mongo \
  -e MONGODB_ADMIN_PASS=mongo \
  -e MONGODB_APPLICATION_DATABASE=cubos \
  -e MONGODB_APPLICATION_USER=mongo \
  -e MONGODB_APPLICATION_PASS=mongo \
  -p 27017:27017 aashreys/mongo-auth:latest`

# Running the application in development mode
`$ npm run dev`

Default port is `8080`

# Endpoints
* `GET /health` to check if the application is up and running

* `POST /rule` to create a new rule
* `GET /rules` to retrieve all the rules currently stored
* `DELETE /rule/:id` to delete a rule using its id

* `GET /schedule` to retrieve the available dates with its intervals. Query: `start: Date (YYYY-MM-DD)` and `end: Date (YYYY-MM-DD)`

# Contracts

Rule:




     {
        "frequency": string ("single", "daily", "weekly", "fortnight", "monthly"),
        
        "date": Date ("YYYY-MM-DD". Required if frequency type is 'single'),

        "days": Array (["monday", "friday"]. Required if frequency type is fortnight, monthly or weekly),
    
        "weeks": Array (1-4. Required if frequency is 'weekly'),
    
        "intervals": [
          {
            "start": Date,
            "end": Date
          }
        ]
      }

# Environment

Name : default

`HTTP_PORT` : `8080`

`MONGO_HOST`: `localhost`

`MONGO_PORT` : `27017`

`MONGO_USER` : `mongo`

`MONGO_PASSWORD` : `mongo`

`MONGO_DB_NAME` : `cubos`

# TODO

tests