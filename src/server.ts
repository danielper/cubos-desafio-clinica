import * as express from "express";
import * as bodyParser from "body-parser";
import { Routes } from "./routes";
import * as mongoose from "mongoose";

const MONGO_HOST = process.env.MONGO_HOST || "localhost";
const MONGO_PORT = process.env.MONGO_PORT || "27017";
const MONGO_USER = process.env.MONGO_USER || "mongo";
const MONGO_PASSWORD = process.env.MONGO_PASSWORD || "mongo";
const MONGO_DB_NAME = process.env.MONGO_DB_NAME || "cubos";

class HttpServer {

    public server: express.Application;
    
    private routes: Routes;
    
    private mongoUrl = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB_NAME}`;

    constructor() {
        this.setupHttp();     
        this.setupDatabase();
    }

    private setupHttp(): void { 
        this.server = express();
        this.server.use(bodyParser.json());
        this.server.use(bodyParser.urlencoded({ extended: false }));
        this.routes = new Routes();
        this.routes.build(this.server);
    }

    private setupDatabase(): void {
        (<any>mongoose).Promise = global.Promise;
        mongoose.connect(this.mongoUrl)
            .catch(err => {
                console.error(`Unable to connect to MongoDB: ${err.errmsg} \nExiting.`);
                process.exit(-1);
            });
    }

}

export default new HttpServer().server;