import server from "./server";

const PORT = 8080 || process.env.HTTP_PORT;

server.listen(PORT, () => {
    console.log(`Application running at: ${PORT}`);
});