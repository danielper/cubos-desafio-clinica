import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export enum RuleFrequency {
    SINGLE = "single",
    DAILY = "daily",
    WEEKLY = "weekly",
    FORTNIGHT = "fortnight",
    MONTHLY = "monthly"
}

export const TimeRuleSchema = new Schema({
    frequency: String,
    date: Date,
    days: [String],
    weeks: [Number],
    intervals: [
        {
            _id: { id: false },
            start: String,
            end: String
        }
    ]
},{ versionKey: false });

export interface TimeRule {
    frequency: string,
    date: Date,
    days: [string],
    weeks: [number],
    intervals: [
        {
            start: string,
            end: string
        }
    ]
}