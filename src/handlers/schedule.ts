import * as mongoose from 'mongoose';
import { TimeRuleSchema, TimeRule } from '../models/timeRule';
import { Request, Response } from 'express';
import ScheduleChecker from '../services/scheduleChecker';
const TimeRule = mongoose.model('TimeRule', TimeRuleSchema);

class ScheduleHandler {
    public findAvailableTime(req: Request, res: Response) {
        TimeRule.find().then((rules) => {
            const data = ScheduleChecker.getAvailable(rules as any, req.query);
            res.send(data)
        }).catch(err => {
            res.status(500).send(err)
            console.error(err);
        });
    }
}

export default new ScheduleHandler();