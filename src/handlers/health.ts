import { Request, Response } from "express";

class HealthHandler {
    public health (req: Request, res: Response) : void {
        res.status(200).send({
            message: 'OK'
        });
    }
}

export default new HealthHandler();