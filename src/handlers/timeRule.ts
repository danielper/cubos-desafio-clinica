import * as mongoose from 'mongoose';
import { TimeRuleSchema } from '../models/timeRule';
import { Request, Response } from 'express';
const TimeRule = mongoose.model('TimeRule', TimeRuleSchema);

class TimeRuleHandler {
    public createRule(req: Request, res: Response) {
        const timeRule = new TimeRule(req.body);

        timeRule.save()
            .then(rule => res.status(201).json(rule))
            .catch(err => res.status(500).send(err));
    }

    public findAllRules(res: Response) {
        TimeRule.find()
            .then(result => res.send(result))
            .catch(err => res.status(500).send(err))
    }

    public removeRule(req: Request, res: Response) {
        TimeRule.findByIdAndRemove(req.params.id).then(result => {
            if (!result)
                return res.status(400).send({ message: "Not found" })
            res.status(200).send(result)
        }).catch(err => res.status(500).send(err));
    }
}

export default new TimeRuleHandler();