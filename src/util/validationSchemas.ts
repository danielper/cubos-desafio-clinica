import { RuleFrequency } from "../models/timeRule";
import * as Joi from 'joi';
import Extension = require('joi-date-extensions');
const DateJoi = Joi.extend(Extension);

const validTimeRegex = /^(([0-9])|([0-1][0-9])|([2][0-3]))(:(([0-9])|([0-5][0-9])))?$/;

export class ValidationSchema {
    public static timeRule = Joi.object().keys({
        frequency: Joi.string().equal(
            RuleFrequency.DAILY,
            RuleFrequency.FORTNIGHT,
            RuleFrequency.MONTHLY,
            RuleFrequency.SINGLE,
            RuleFrequency.WEEKLY).required(),
        date: Joi.date()
            .when('frequency', {
                is: Joi.equal(RuleFrequency.SINGLE),
                then: Joi.required()
            }),
        days: Joi.array()
            .items(DateJoi.date().format('dddd')).raw(true)
            .when('frequency', {
                is: Joi.equal(
                    RuleFrequency.FORTNIGHT ||
                    RuleFrequency.MONTHLY ||
                    RuleFrequency.WEEKLY),
                then: Joi.required()
            }),
        weeks: Joi.array()
            .items(Joi.number().min(1).max(4))
            .when('frequency', {
                is: Joi.equal(RuleFrequency.FORTNIGHT),
                then: Joi.array().length(2).unique((a, b) => !(a - b === 2 || b - a === 2)).required()
            }).label("Weeks must be diffent by 2"),
        intervals: Joi.array().items(Joi.object().keys({
            start: Joi.string().regex(validTimeRegex).required(),
            end: Joi.string().regex(validTimeRegex).required()
        })).required(),
    });

    public static timeRange = Joi.object().keys({
        start: DateJoi.date()
            .max(Joi.ref("end"))
            .required(),
        end: DateJoi.date()
            .min(Joi.ref("start"))
            .required()
    }).and("start", "end");
}