import { TimeRule, RuleFrequency } from "../models/timeRule";
import Moment = require("moment");
import * as recur from "moment-recur-ts";
import { extendMoment } from 'moment-range';
import { string } from "joi";
require('moment-recur');
const moment = extendMoment(Moment);

interface Filters {
    start: Date,
    end: Date
}

interface DateIntervals {
    date: string,
    intervals: Array<Interval>
}

interface Interval {
    start: string,
    end: string
}

export default class ScheduleChecker {
    public static getAvailable(source: TimeRule[], filters: Filters) {
        let result = new Map<string, Array<Interval>>();
        source.forEach(rule => {
            switch (rule.frequency) {
                case RuleFrequency.MONTHLY || RuleFrequency.FORTNIGHT: {
                    const filteredIntervals = ScheduleChecker.getMonthly(rule, filters);
                    result = ScheduleChecker.appendInterval(result, ...filteredIntervals);
                }; break;
                case RuleFrequency.WEEKLY: {
                    const filteredIntervals = ScheduleChecker.getWeekly(rule, filters);
                    result = ScheduleChecker.appendInterval(result, ...filteredIntervals);
                }; break;
                case RuleFrequency.DAILY: {
                    const dateInterval = {
                        date: moment(rule.date).format('YYYY-MM-DD'),
                        intervals: rule.intervals
                    };
                    result = ScheduleChecker.appendInterval(result, dateInterval);
                }; break;
                case RuleFrequency.SINGLE: {
                    const filteredIntervals = ScheduleChecker.getSingle(rule, filters);
                    result = ScheduleChecker.appendInterval(result, ...filteredIntervals);
                }; break;
            }
        })

        const sortedResult = new Map([...result.entries()].sort());
        const localizedResult = ScheduleChecker.parseDatesToLocal(sortedResult);
        return ScheduleChecker.fromMapDistinct(localizedResult);
    }

    private static parseDatesToLocal(source : Map<string, Array<Interval>>):Map<string, Array<Interval>>{
        const result = new Map<string, Array<Interval>>();
        source.forEach((value, key)=>{
            result.set(moment(key).format("DD-MM-YYYY"), value);
        });

        return result;
    }

    private static fromMapDistinct(a: Map<string, Array<Interval>>): Array<DateIntervals> {
        const result = new Array<DateIntervals>();
        a.forEach((value, key) => result.push({
            date: key,
            intervals: [...new Set(value.map(o => JSON.stringify(o)))].map(s => JSON.parse(s))
        }));

        return result;
    }


    private static appendInterval(dest: Map<string, Array<Interval>>, ...source: Array<DateIntervals>): Map<string, Array<Interval>> {
        const result = dest;
        source.forEach(interval => {
            if (dest.has(interval.date)) {
                result.set(interval.date, dest.get(interval.date).concat(interval.intervals))
            } else {
                result.set(interval.date, interval.intervals);
            }
        });

        return result;
    }

    private static getMonthly(rule: TimeRule, filter: Filters): Array<DateIntervals> {
        const intervals = new Array<DateIntervals>();
        const start = moment(filter.start).utc();
        const end = moment(filter.end).utc();
        const recurrence = start.recur().every(rule.days).daysOfWeek()
            .every(rule.weeks.map(w => w - 1)).weeksOfMonthByDay();
        const range = moment.range(start.dateOnly(), end.dateOnly());

        for (const day of range.by('day')) {
            const date = day.format('YYYY-MM-DD');
            if (recurrence.matches(date))
                intervals.push({ date, intervals: rule.intervals })
        }

        return intervals;
    }

    private static getWeekly(rule: TimeRule, filter: Filters): Array<DateIntervals> {
        const intervals = new Array<DateIntervals>();
        const start = moment(filter.start).utc();
        const end = moment(filter.end).utc();
        const recurrence = start.recur().every(rule.days).daysOfWeek();
        const range = moment.range(start.dateOnly(), end.dateOnly());

        for (const day of range.by('day')) {
            const date = day.format('YYYY-MM-DD');
            if (recurrence.matches(date))
                intervals.push({ date, intervals: rule.intervals })
        }

        return intervals;
    }

    private static getSingle(rule: TimeRule, filter: Filters): Array<DateIntervals> {
        const intervals = new Array<DateIntervals>();
        const start = moment(filter.start).utc();
        const end = moment(filter.end).utc();
        const range = moment.range(start.dateOnly(), end.dateOnly());
        const date = moment(rule.date);
        if (range.contains(date.dateOnly())) {
            intervals.push({ date: date.format('YYYY-MM-DD'), intervals: rule.intervals })
        }

        return intervals;
    }
}