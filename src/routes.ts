import { Application } from "express";
import schedule from "./handlers/schedule";
import health from "./handlers/health";
import timeRule from "./handlers/timeRule";
import { ValidationSchema } from "./util/validationSchemas";
const validator = require('express-joi-validation')({});

export class Routes {
    public build(app: Application): void {
        app.get('/health', health.health);
        
        app.get('/rules', timeRule.findAllRules);
        app.post('/rule', validator.body(ValidationSchema.timeRule), timeRule.createRule);
        app.delete('/rule/:id', timeRule.removeRule);

        app.get('/schedule/', validator.query(ValidationSchema.timeRange), schedule.findAvailableTime)
    }
}

